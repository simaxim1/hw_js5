'use strict'

/* 
    1. Опишіть своїми словами, що таке метод об'єкту.
        Це властивість об'єкту, яка виражена у вигляді функції. 

    2. Який тип даних може мати значення властивості об'єкта?
        Будь-який, дозволенний у JavaScript.

    3. Об'єкт це посилальний тип даних. Що означає це поняття?
        Змінні мають у якості значень тільки посилання на якесь значення замість зберігання фактичних значень.
         Тому різні змінні можуть мати посилання на одне збережене значення. Приклад:

        let obj = {name : 'Den'}
        let obj2 = obj;
        obj2.name = 'Sam';

        console.log(obj.name); // Sam

*/

const createNewUser = () => {

    const firstName = prompt('Add your name');
    const lastName  = prompt('Add your Last Name');

    let object = {};

    Object.defineProperties(object, {
        firstName: {

            value: firstName,
            writable: false,

        },
          
        lastName: {

            value: lastName,
            writable: false,

        },

        getLogin:  {
        
            value: function() {

            return this.firstName.toLowerCase()[0] + this.lastName.toLowerCase();
        }},

    });

    return object;

};

const newUser = createNewUser();

console.log(`Your login is: ${newUser.getLogin()}`);

newUser.firstName = 'John'; // or newUser.lastName  = 'Smith';
